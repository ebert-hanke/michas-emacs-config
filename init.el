;;; michas emacs config - 2021
;;; this config leans more to be used with no window emacs (emacs -nw)

;;; thanks and credits

;; I am by no means an expert on Emacs. Everything I know I slowly cobbled together from different resources.
;; There is a lot of good stuff out there about Emacs and many great people devoted lots of energy
;; to compiling all that knowledge.
;; Many of the things you find in this file are taken from one of these resources or based upon it
;; so I want to give due credit and say thank you! 

;; - the Emacs Berlin Meetup
;; -> https://emacs-berlin.org/
;; - Gregory J Stein
;; -> [[https://github.com/gjstein][Gregory J Stein]]
;; - Mike Zamansky 
;; -> [[https://cestlaz.github.io][Mike Zamansky]] 
;; - Sacha Chua
;; -> [[https://sachachua.com/blog/][Sacha Chua]]
;; - Rainer König
;; -> [[https://www.youtube.com/playlist?list=PLVtKhBrRV_ZkPnBtt_TD1Cs9PJlU0IIdE][Rainer König]]
;; - Pragmatic Emacs
;; -> [[http://pragmaticemacs.com/][Pragmatic Emacs]]
;; - The Woodnotes Guide to Emacs for Writers
;; -> http://therandymon.com/woodnotes/emacs-for-writers/emacs-for-writers.html
;; - Uncle Daves Youtube Channel
;; -> https://www.youtube.com/channel/UCDEtZ7AKmwS0_GNJog01D2g
;; - Mohammed Ismail Ansari's Emacs Config in 24 min
;; -> https://www.youtube.com/watch?v=FRu8SRWuUko
;; - Franziskas WebDev Setup
;; -> https://fransiska.github.io/emacs/2017/08/21/web-development-in-emacs
;; - Arjen Wiersma's GNU Emacs configuration for programming
;; -> https://www.youtube.com/watch?v=I28jFkpN5Zk
;; - Mastering Emacs
;; -> https://www.masteringemacs.org/
;; - Robert Krahn
;; -> https://robert.kra.hn/posts/2021-02-07_rust-with-emacs/
;; - ... and lots of others :)
;; - and of course all the great documentation which I will do my best to link to in the appropriate places of this config
;; - obviously this list cant be complete without thanking Richard Stallman (together with Guy L. Steele Jr. and others)
;; for GNU Emacs itself 
;; - and because its fun: the church of Emacs
;; -> https://www.youtube.com/watch?v=Gnnb6sjgk3A

;;; this config uses use-package

;; initializing package with repositiories and make sure use-package is present
(require 'package)
(add-to-list 'package-archives '("gnu"   . "https://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/"))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-and-compile
  (setq use-package-always-ensure t
        use-package-expand-minimally t))

;; always ensure packages to autoinstall if missing
;; if you don't like this you can ":ensure t" the packages individually
(require 'use-package-ensure)
(setq use-package-always-ensure t)

;;; os specific setup
;; fixing keys and dired ls
(when (string= system-type "darwin")       
  (set-keyboard-coding-system nil)
  (setq mac-option-key-is-meta nil
	mac-command-key-is-meta t
	mac-command-modifier 'meta
	mac-option-modifier 'none)
  (setq ns-function-modifier 'super)
  (setq dired-use-ls-dired nil))

;;; general customizations

;; separate custom stuff
(setq custom-file (concat user-emacs-directory "custom.el"))
(when (file-exists-p custom-file)
  (load custom-file))

;; cl is deprecated, we know ...
(setq byte-compile-warnings '(cl-functions))

;; exec-path
;; if you are not in terminal your $PATH might not work inside emacs
;; add needed folders
(use-package exec-path-from-shell
  :config
  (when (memq window-system '(mac ns x))
    (exec-path-from-shell-initialize)))

;; no loadtime in modeline
(setq display-time-default-load-average nil)

;; no startup screen
(setq inhibit-startup-screen t)

;; y or n
(defalias 'yes-or-no-p 'y-or-n-p)

;; visible bell / ring bell
(setq visible-bell nil)
(setq ring-bell-function 'ignore)

;; no menu-, tool- and scrollbar
(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)

;; utf-8 please
(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

;;; usability

;; visit config file
(defun config-visit ()
  (interactive)
  (find-file "~/.emacs.d/init.el"))
(global-set-key (kbd "C-c e") 'config-visit)

;; switch between recently used buffers
(global-set-key (kbd "M-o")  'mode-line-other-buffer)

;; navigate between windows
;; use shift-arrowkey to navigate when possible otherwise M-x o
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))

;; line numbers
(setq display-line-numbers-type 'relative) 
(add-hook 'prog-mode-hook 'display-line-numbers-mode)

;; highlight matchin parens
(show-paren-mode 1)

;; electric pair
(electric-pair-mode 1)

;; always kill current buffer
(defun kill-curr-buffer ()
  (interactive)
  (kill-buffer (current-buffer)))
(global-set-key (kbd "C-x k") 'kill-curr-buffer)

;; prettify symbols
(global-prettify-symbols-mode t)

;; dired settings
(put 'dired-find-alternate-file 'disabled nil) ; press 'a' to reuse dired buffer
(setq dired-dwim-target t) ; dired target is other split

;; ;; desktop-save-mode
;; ;; Automatically save and restore sessions
;; (setq desktop-dirname "~/.emacs.d/desktop/") ; desktops save file folder
;; (setq desktop-base-file-name "emacs.desktop") 
;; (setq desktop-base-lock-name "lock")
;; (setq desktop-path (list desktop-dirname))
;; (setq desktop-save t)
;; (setq desktop-load-locked-desktop nil)
;; (setq desktop-auto-save-timeout 30) ; autosave every 30s idle
;; (desktop-save-mode 1)

;; autosave
(setq auto-save-interval 20)

;; backups
(setq
 backup-by-copying t    
 kept-new-versions 10  
 kept-old-versions 0  
 delete-old-versions t 
 version-control t    
 vc-make-backup-files t)

(setq backup-directory-alist
          `(("." . ,(concat user-emacs-directory "backups"))))

;; spell checking flyspell
;; https://github.com/kaushalmodi/.emacs.d/blob/master/setup-files/setup-spell.el
(add-hook 'text-mode-hook 'flyspell-mode)
(setq flyspell-correct-interface 'flyspell-correct-helm)
;;; packages

;; general packages

;; ripgrep
;; requires ripgrep
(use-package ripgrep)

;; deligt
(use-package delight)

;; diminish
(use-package diminish)

;; font
;; i recommend using a nerd font like SauceCodePro Nerd Font
;; https://www.nerdfonts.com
;; setting it for window mode emacs
(when (display-graphic-p)
  (when (member "SauceCodePro Nerd Font" (font-family-list))
    (set-frame-font "SauceCodePro Nerd Font-16" t t)))

;; theme for emacs -nw
(unless (display-graphic-p)
(use-package zenburn-theme
  :config
  (load-theme 'zenburn t))
)

;; fancy modeline for gui
(when (display-graphic-p)

  ;; required for doom-modeline
  (use-package all-the-icons)
  
  ;; doom-modeline
  (use-package doom-modeline
    :hook (after-init . doom-modeline-mode))

  ;; doom-themes for modeline
  (use-package doom-themes
    :ensure t
    :config
    ;; Global settings (defaults)
    (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
          doom-themes-enable-italic t) ; if nil, italics is universally disabled
    (load-theme 'doom-zenburn t)
    ;; Enable flashing mode-line on errors
    (doom-themes-visual-bell-config)
    ;; Enable custom neotree theme (all-the-icons must be installed!)
    ;;(doom-themes-neotree-config)
    ;; or for treemacs users
    ;;(setq doom-themes-treemacs-theme "doom-atom") ; use "doom-colors" for less minimal icon theme
    ;;(doom-themes-treemacs-config)
    ;; Corrects (and improves) org-mode's native fontification.
    (doom-themes-org-config))
)

;; evil
(use-package evil
     :init
     (setq evil-want-integration t) 
     (setq evil-want-keybinding nil)
     (setq evil-want-C-i-jump nil) ; fix tab in org mode
     :config
     (evil-mode 1)
    )

;; change the evil cursor depending on state in emacs -nw
;; https://www2.ccs.neu.edu/research/gpc/VonaUtils/vona/terminal/vtansi.htm
;; https://github.com/h0d/term-cursor.el/blob/master/term-cursor.el
;; 0, 2- box
;; 1 - blinking box
;; 3 - blinking underline
;; 4 - steady underline
;; 5 - blinking bar
;; 6 - steady bar
(unless (display-graphic-p)
  (add-hook 'evil-insert-state-entry-hook (lambda () (send-string-to-terminal "\033[5 q")))
  (add-hook 'evil-normal-state-entry-hook (lambda () (send-string-to-terminal "\033[0 q")))
)

;; evil-collection
(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

;; evil-escape
(use-package evil-escape
  :after evil
  :diminish
  :config
  (evil-escape-mode 1)
  (setq-default evil-escape-key-sequence "ii")
  (setq-default evil-escape-delay 0.4))

;; vterm
;; in order to have multiple terminal buffers they need to be named
;; this can be done from shell with a function, see below
(use-package vterm
  :after evil-collection
  :config
  (setq vterm-buffer-name-string "vterm %s")
  :bind ("C-c t" . vterm))
;; function for fish shell
;; # set vterm title via fish
;; function fish_title
;;   #  hostname
;;   #  echo ":"
;;     pwd
;; end

;; perspecive
;; (use-package perspective
;;   :init
;;   (setq persp-state-default-file "~/.emacs.d/perspective.save") ; perspective save file
;;   :config
;;   (add-hook 'kill-emacs-hook #'persp-state-save) ; autosave
;;   (persp-mode))
;; ;
					;(add-hook 'after-init-hook (lambda ()(persp-state-load persp-state-default-file))) ; autoload on startup

;; ;; trying out burly, buffler and quelpa
;; ;; bootstrap quelpa
;; ;; (unless (package-installed-p 'quelpa)
;; ;;   (with-temp-buffer
;; ;;     (url-insert-file-contents "https://raw.githubusercontent.com/quelpa/quelpa/master/quelpa.el")
;; ;;     (eval-buffer)
;; ;;     (quelpa-self-upgrade)))
;; ;; ;

;; 					; quelpa-use-package
;; (quelpa
;;  '(quelpa-use-package
;;    :fetcher git
;;    :url "https://github.com/quelpa/quelpa-use-package.git"))
;; (require 'quelpa-use-package)
;; ;; bufler
;; ;;(use-package bufler
;; ;;  :quelpa (bufler :fetcher github :repo "alphapapa/bufler.el"
;; ;;                  :files (:defaults (:exclude "helm-bufler.el"))))
;; ;; helm-bufler
;; ;; (use-package helm-bufler
;; ;;   :quelpa (helm-bufler :fetcher github :repo "alphapapa/bufler.el"
;; ;;                        :files ("helm-bufler.el")))

;; ;; burly
;; (use-package burly
;;   :quelpa (burly :fetcher github :repo "alphapapa/burly.el"))

;; which key
(use-package which-key
  :config
  (which-key-mode))

;; projectile
(use-package projectile
  :init
  (projectile-mode +1)
  :bind (:map projectile-mode-map
              ("s-p" . projectile-command-map)
              ("C-c p" . projectile-command-map)))

;; magit
;; install manually
(use-package magit
  :config
  (global-set-key (kbd "C-x g") 'magit-status))

;; swiper
(use-package swiper
  :bind ("C-s" . 'swiper))

;; flycheck
(use-package flycheck
  :diminish
  :init 
  (global-flycheck-mode))

;; yasnippet
(use-package yasnippet
  :diminish yas-minor-mode
  :config
  (setq yas-snippet-dirs
	(append yas-snippet-dirs
		'(
		  "~/.emacs.d/snippets/rustic-mode" ;; add your per mode snippet collections here 
		  )))
  (yas-reload-all)
  (add-hook 'prog-mode-hook 'yas-minor-mode)
  (add-hook 'text-mode-hook 'yas-minor-mode))
 
;; company
(use-package company
  :after lsp-mode
  :hook (lsp-mode . company-mode)
  :custom
  (company-idle-delay 0.5) ;; how long to wait until popup
  :bind
  (:map company-active-map
	      ("C-n". company-select-next)
	      ("C-p". company-select-previous)
	      ("M-<". company-select-first)
	      ("M->". company-select-last))
  (:map company-mode-map
	("<tab>". tab-indent-or-complete)
	("TAB". tab-indent-or-complete)))

(defun company-yasnippet-or-completion ()
  (interactive)
  (or (do-yas-expand)
      (company-complete-common)))

(defun check-expansion ()
  (save-excursion
    (if (looking-at "\\_>") t
      (backward-char 1)
      (if (looking-at "\\.") t
        (backward-char 1)
        (if (looking-at "::") t nil)))))

(defun do-yas-expand ()
  (let ((yas/fallback-behavior 'return-nil))
    (yas/expand)))

(defun tab-indent-or-complete ()
  (interactive)
  (if (minibufferp)
      (minibuffer-complete)
    (if (or (not yas/minor-mode)
            (null (do-yas-expand)))
        (if (check-expansion)
            (company-complete-common)
          (indent-for-tab-command)))))

;; ;; company-box
;; (use-package company-box
;;   :hook (company-mode . company-box-mode))

;; helm
(use-package helm
  :diminish
  :bind
  ("C-x C-f" . 'helm-find-files)
  ("C-x C-b" . 'helm-buffers-list)
  ("M-x" . 'helm-M-x)
  ("C-x r b" . 'helm-bookmarks)
  :init
  (helm-mode 1))

(require 'helm-config)

;; helm-projectile
(use-package helm-projectile
  :config
  (helm-projectile-on))

;; lsp-mode
;; please install the required language servers
(use-package lsp-mode
  :init
  :hook  ((web-mode . lsp)
          (js2-mode . lsp)            
          (js-mode . lsp)            
          (scss-mode . lsp)
          (sass-mode . lsp)
          (json-mode . lsp)
          (lsp-mode . lsp-enable-which-key-integration)
          (lsp-mode . lsp-ui-mode))

  :custom
  (lsp-rust-analyzer-cargo-watch-command "clippy")
  (lsp-eldoc-render-all t)
  (lsp-idle-delay 0.5)
  ;; (lsp-rust-analyzer-server-display-inlay-hints t) 
  
  :config
  (setq lsp-signature-auto-activate nil)
  (setq lsp-signature-render-document nil)
  (setq lsp-eldoc-enable-hover nil)
  (setq lsp-log-io nil) ; if set to true can cause a performance hit 

  :commands lsp lsp-deferred)

;; lsp-ui
(use-package lsp-ui
  :commands lsp-ui-mode
  :custom
  (lsp-ui-peek-always-show t)
  (lsp-ui-sideline-show-hover t)
  (lsp-ui-doc-enable nil))

;; helm-lsp
(use-package helm-lsp
  :commands helm-lsp-workspace-symbol)

;; more flycheck output
;; (with-eval-after-load 'lsp-mode
;;   (mapc #'lsp-flycheck-add-mode '(js-mode js2-mode css-mode sass-mode scss-mode web-mode)))

;; lsp tuning
(setq gc-cons-threshold 100000000)
(setq read-process-output-max (* 2 1024 1024)) ;; 2mb
(setq lsp-file-watch-threshold 2000)	       

;; rainbow-delimeters
(use-package rainbow-delimiters
  :hook
  (prog-mode . rainbow-delimiters-mode))

;; prettier-js
;; prettier must be installed with `npm install -g prettier`
(use-package prettier-js
  :init
  (setq prettier-js-args '(
			   "--single-quote" "true"
			   "--prose-wrap" "never"
			   "--trailing-comma" "none"
			   ))
  ;; (defun enable-minor-mode (my-pair)
  ;;   "Enable minor mode if filename match the regexp.  MY-PAIR is a cons cell (regexp . minor-mode)."
  ;;   (if (buffer-file-name)
  ;;       (if (string-match (car my-pair) buffer-file-name)
  ;;           (funcall (cdr my-pair)))))
  :hook(
   (js2-mode . prettier-js-mode)
   (web-mode . prettier-js-mode)
   (scss-mode . prettier-js-mode)
   )
   ;; (add-hook 'web-mode-hook #'(lambda ()
   ;; 				(enable-minor-mode
   ;; 				 '("\\.jsx?\\'" . prettier-js-mode))))
   )

;;; language specific packages

;; markdown-mode
(use-package markdown-mode
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

;; toml-mode
(use-package toml-mode)

;; yaml-mode
(use-package yaml-mode
:mode (("\\.yml$" . yaml-mode)))
(add-hook 'yaml-mode-hook
	  '(lambda ()
             (define-key yaml-mode-map "\C-m" 'newline-and-indent)))

;; json-mode
(use-package json-mode)

;; scss-mode
(use-package scss-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.scss\\'" . scss-mode)))

;; js2-mode
(use-package js2-mode
  :init
  (add-hook 'js2-mode-hook #'js2-imenu-extras-mode)
  :config
  (add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
  (setq js2-basic-offset 2)
  (setq indent-tabs-mode nil))

;; emmet-mode
(use-package emmet-mode
  :hook(
  (web-mode . emmet-mode)
  (sgml-mode . emmet-mode)
  ))

;; web-mode
(use-package web-mode
  :mode (("\\.erb\\'" . web-mode)
         ("\\.mustache\\'" . web-mode)
         ("\\.html?\\'" . web-mode)
         ("\\.php\\'" . web-mode)
         ("\\.vue\\'" . web-mode))
 ;; :hook (scss-mode . web-mode)
  :config (progn
            (setq web-mode-markup-indent-offset 2
                  web-mode-css-indent-offset 2
                  web-mode-code-indent-offset 2)))

;; rustic
(use-package rustic
  :config
  (setq rustic-format-on-save t)
  (push 'rustic-clippy flycheck-checkers)
  (setq rustic-flycheck-clippy-params "--message-format=json")
  )

;;; writing

;; spell checking guess language
(use-package guess-language
  :init
  (setq guess-language-langcodes
	'((en . ("en_US" "English"))
	  (de . ("de_DE" "German"))))
  (setq guess-language-languages '(en de))
  (setq guess-language-min-paragraph-length 35)
  :hook(
	(org-mode . guess-language-mode)
	(text-mode . guess-language-mode)
	))

;; org mode settings

;; general settings
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(setq org-log-done t)
;; soft wrap lines and indent for org mode
(with-eval-after-load 'org       
  (setq org-startup-indented t) 
  (add-hook 'org-mode-hook 'visual-line-mode)
  (add-hook 'org-mode-hook 'flyspell-mode))
;; adjust time format for clocksum in column view
(setq org-duration-format 'h:mm)
(setq org-time-clocksum-format (quote (:hours "%d" :require-hours t :minutes ":%02d" :require-minutes t)))
;; clock into drawer CLOCKING
(setq org-clock-into-drawer "CLOCKING")

;; org-autolist
(use-package org-autolist
  :hook
  (org-mode . org-autolist-mode))

;; latex things

;; of course latex needs to be installed: https://www.latex-project.org/get/
;; thanks to all contributors to this reddit thread
;; found here: https://www.reddit.com/r/emacs/comments/cd6fe2/how_to_make_emacs_a_latex_ide/
;; also hunspell and the relevant dictionaries need to be installed
(use-package pdf-tools
  :ensure t
  :config
  (pdf-tools-install)
  (setq-default pdf-view-display-size 'fit-page)
  (setq pdf-annot-activate-created-annotations t)
  (define-key pdf-view-mode-map (kbd "C-s") 'isearch-forward)
  (define-key pdf-view-mode-map (kbd "C-r") 'isearch-backward)
  ;;(add-hook 'pdf-view-mode-hook (lambda ()
;;				  (bms/pdf-midnite-amber))) ; automatically turns on midnight-mode for pdfs
  )

(use-package auctex-latexmk
  :ensure t
  :config
  (auctex-latexmk-setup)
  (setq auctex-latexmk-inherit-TeX-PDF-mode t))

(use-package reftex
  :ensure t
  :defer t
  :config
  (setq reftex-cite-prompt-optional-args t)) ;; Prompt for empty optional arguments in cite

(use-package company-auctex
  :ensure t
  :init (company-auctex-init))

(use-package tex
  :ensure auctex
  :mode ("\\.tex\\'" . latex-mode)
  :config (progn
	    (setq TeX-source-correlate-mode t)
	    (setq TeX-source-correlate-method 'synctex)
	    (setq TeX-auto-save t)
	    (setq TeX-parse-self t)
	    (setq-default TeX-master nil)
	    ;;(setq-default TeX-master "paper.tex")
	    (setq reftex-plug-into-AUCTeX t)
	    (pdf-tools-install)
	    (setq TeX-view-program-selection '((output-pdf "PDF Tools"))
		  TeX-source-correlate-start-server t)
	    ;; Update PDF buffers after successful LaTeX runs
	    (add-hook 'TeX-after-compilation-finished-functions
		      #'TeX-revert-document-buffer)
	    (add-hook 'LaTeX-mode-hook
		      (lambda ()
			(reftex-mode t)
			(flyspell-mode t)
			(lsp-deferred)))
	    ))

;;; ===== end of config =====
